package com.hendisantika.springboottransactionmanagement.repository;

import com.hendisantika.springboottransactionmanagement.entity.Notes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.44
 */
@Repository
public interface NotesRepository extends CrudRepository<Notes, Long> {
}

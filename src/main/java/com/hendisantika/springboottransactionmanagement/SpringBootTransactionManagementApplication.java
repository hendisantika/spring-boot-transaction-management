package com.hendisantika.springboottransactionmanagement;

import com.hendisantika.springboottransactionmanagement.entity.Notes;
import com.hendisantika.springboottransactionmanagement.entity.User;
import com.hendisantika.springboottransactionmanagement.service.UserNotesLinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableJpaAuditing
@RestController
public class SpringBootTransactionManagementApplication {
    @Autowired
    private UserNotesLinkService userNotesService;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTransactionManagementApplication.class, args);
    }

    @Bean
    public CommandLineRunner initData() {
        return (args) -> {
            /* TEST DATA FOR TRANSACTION MANAGEMENT EXAMPLE*/
            // create new user
            User user = new User();
            user.setUserMail("uzumaki_naruto@konohagakure.co.jp");
            user.setUserPass("12345678jhg");

            //create new note
            Notes note = new Notes();
            note.setTitle("Test Note");
            note.setMessage("Test Message");

            //link above new user with above note
            userNotesService.addNoteToSpecificUser(user, note);
        };
    }


}

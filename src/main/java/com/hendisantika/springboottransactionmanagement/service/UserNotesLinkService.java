package com.hendisantika.springboottransactionmanagement.service;

import com.hendisantika.springboottransactionmanagement.entity.Notes;
import com.hendisantika.springboottransactionmanagement.entity.User;
import com.hendisantika.springboottransactionmanagement.util.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.49
 */
@Service
public class UserNotesLinkService {
    @Autowired
    private UserService userService;

    @Autowired
    private NotesService notesService;

    @Transactional
    public String addNoteToSpecificUser(User user, Notes note) throws Exception {
        //create new user
        User createdUser = userService.registerUser(user);
        Notes dbNote = new Notes();
        dbNote.setTitle(note.getTitle());
        dbNote.setMessage(note.getMessage());
        //set created user to note
        dbNote.setUserDetails(createdUser);
        //persist new note
        notesService.addNote(dbNote);
        return ApplicationConstants.ADDED_NOTE_DESC;
    }
}

package com.hendisantika.springboottransactionmanagement.service;

import com.hendisantika.springboottransactionmanagement.entity.Notes;
import com.hendisantika.springboottransactionmanagement.repository.NotesRepository;
import com.hendisantika.springboottransactionmanagement.util.ApplicationConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.45
 */
@Service
@Log4j2
public class NotesService {

    @Autowired
    private NotesRepository noteRepository;

    public String addNote(Notes note) {
        log.info("Inside add note service");
        noteRepository.save(note);
        log.info("Successfully added new Note.");
        return ApplicationConstants.ADDED_NOTE_DESC;
    }
}

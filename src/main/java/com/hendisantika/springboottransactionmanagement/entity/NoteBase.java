package com.hendisantika.springboottransactionmanagement.entity;

import com.hendisantika.springboottransactionmanagement.util.ApplicationConstants;
import lombok.Data;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.43
 */
@MappedSuperclass
@Data
public class NoteBase {

    @NotEmpty(message = ApplicationConstants.VALIDATION_TITLE_EMPTY)
    @NotBlank(message = ApplicationConstants.VALIDATION_TITLE_EMPTY)
    @Size(max = 50)
    private String title;

    @Size(max = 1000)
    private String message;
}

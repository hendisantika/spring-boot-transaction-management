package com.hendisantika.springboottransactionmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.37
 */
@Entity
@Table(name = "users")
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    @JsonIgnore
    private Long userId;

    @NotNull
    @Email
    @Size(max = 100)
    @Pattern(regexp = ".+@.+\\..+", message = "Wrong email!")
    @Column(name = "user_mail", unique = true)
    @JsonProperty("Mail Id")
    private String userMail;

    @NotNull
    @Size(min = 8, message = "Password should have atleast 8 characters")
    @Column(name = "user_pass")
    @JsonProperty("Password")
    private String userPass;

}

package com.hendisantika.springboottransactionmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hendisantika.springboottransactionmanagement.util.ApplicationConstants;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-transaction-management
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/04/21
 * Time: 04.41
 */
@Entity
@Table(name = "Notes")
@Data
public class Notes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "note_id")
    private Long noteId;


    /*In case of we need to maintain many notes to one User relation through User Entity*/
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userDetails;

    @NotEmpty(message = ApplicationConstants.VALIDATION_TITLE_EMPTY)
    @NotBlank(message = ApplicationConstants.VALIDATION_TITLE_EMPTY)
    @Size(max = 50)
    private String title;

    @Size(max = 1000)
    private String message;
}

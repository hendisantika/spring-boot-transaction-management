# Spring Boot Transaction Management

### What is database transaction?

> A database transaction is a sequence of actions that are treated as a single unit of work. Transaction management is an essential aspect of RDBMS application to ensure data integrity and consistency. The transaction can be defined with ACID properties.

1. Atomicity - All success or none.
2. Consistency - Integrity constraints must be maintained in such a way that the database is consistent before and after
   the transaction.
3. Isolation - One transaction should not impact on another transaction.
4. Durability - After completion of the transaction, the changes and modifications to the database will be stored in and
   written to the disk and will continue even if a system failure occurs.

There are 2 ways to achieve transaction management in Spring:

Spring Programmatic Transaction Management

1. With programmatic transactions, transaction management code needs to be explicitly written so as to commit when
   everything is successful and rolling back if anything goes wrong. The transaction management code is tightly bound to
   the business logic in this case.

```java
UserNoteTransaction userNoteTransaction = entityManager.getTransaction();
try {
   //Begin Transaction
    userNoteTransaction.begin();

    /* register user - query 1
	 create note
	link note to user - query 2 */

    //Commit Transaction
    userNoteTransaction.commit();
} catch(Exception exception) {
    //Rollback Transaction
    userNoteTransaction.rollback();
    throw exception;
} 
```

2. Spring Declarative Transaction Management

   Using @Transactional, the above code gets reduced to simply this:
   ```java
    @Transactional
    public void addNoteToSpecificUser() {
    /* register user - query 1
    create note
    link note to user - query 2 */
    }
    ```
> In Transaction Management, different types of Transaction Propagation specifies whether or not the corresponding component
> will participate in the transaction and what happens if the calling component/service has or does not already have a transaction created/started.